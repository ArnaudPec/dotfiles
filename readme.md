My configuration files.

Use GNU stow to deploy.

Depends on
```sh
nvim
X
bspwm
sxhkd
cava
tmux
zsh
moc
yabar
highlight
stow
```

Debian :

On a debian host

    ./install.sh and execute the install functions

    # User helper
    user="user"
    name="User"
    useradd -m -g sudo -c $name -s /bin/$user
    passwd $user

    echo "$user ALL=(ALL) NOPASSWD: /path/to/script" >> /etx/sudoers
    # check internet connection

    echo -e "sudo    ALL=(ALL:ALL) ALL" >> /etc/sudoers
