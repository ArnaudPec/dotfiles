set nocompatible
filetype off

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.nvim/plugged')

Plug 'scrooloose/nerdcommenter'
Plug 'Raimondi/delimitMate'
Plug 'godlygeek/tabular'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
" Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'lilydjwg/colorizer'
Plug 'vim-scripts/taglist.vim'
Plug 'vim-scripts/DrawIt'
Plug 'folke/zen-mode.nvim'

" Markdown specific
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }

" Python specifics
Plug 'Shougo/deoplete.nvim', { 'for': 'python,cpp', 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi', { 'for': 'python' }
Plug 'vim-scripts/indentpython.vim', { 'for': 'python' }
Plug 'vim-syntastic/syntastic', { 'for': 'python' }
Plug 'nvie/vim-flake8', { 'for': 'python' }

"Plug 'autozimu/LanguageClient-neovim', {
"    \ 'branch': 'next',
"    \ 'do': 'bash install.sh',
"    \ 'for': 'cpp',
"    \ }

call plug#end()

" let g:deoplete#enable_at_startup = 1
" let python_highlight_all=1
" 
" let g:LanguageClient_serverCommands = {
"   \ 'cpp': ['clangd-9'],
"   \ }
" let g:LanguageClient_autoStart = 1
" 
" nnoremap <silent> <C-d> :call LanguageClient_textDocument_definition()      <CR>
" nnoremap <silent> <C-h> :call LanguageClient_textDocument_hover()           <CR>
" nnoremap <silent> <C-n> :call LanguageClient#textDocument_rename()          <CR>
" nnoremap <silent> <C-s> :call LanguageClient#textDocument_documentSymbol()  <CR>

set noshowmode

set timeoutlen=600

" DelimitMate options: let expand when we press enter key
let delimitMate_expand_cr = 1

" ultisnips options
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-k><c-i>"

let g:limelight_conceal_ctermfg = 'black'
let g:limelight_paragraph_span = 1

filetype plugin on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Search
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set show matching parenthesis
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=1
" Ignore case when searching
set ignorecase
" Ignore case if search pattern is all lowercase, case-sensitive otherwise
set smartcase
" Insert tabs on the start of a line according to shiftwidth, not tabstop
set smarttab
" Highlight search terms
set hlsearch
" Show search matches as you type
set incsearch

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Allow backspacing over everything in insert mode
set backspace=indent,eol,start
" Use spaces instead of tabs
set expandtab
" Always set autoindenting on
set autoindent
" Copy the previous indentation on autoindenting
set copyindent
" Use multiple of shiftwidth when indenting with '<' and '>'
set shiftround
" Don't wrap lines
set nowrap
" Number of spaces to use for autoindenting
set shiftwidth=4
" A tab is four spaces
set tabstop=4
" Enable indent
filetype indent on
" Disable auto-comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  File browsing with built-in plugin
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Diable banner
let g:netrw_banner=0
" Open in v split by default
let g:netrw_browser_split=2
" tree style list
let g:netrw_liststyle=3
" Open vsplits to the right
let g:netrw_altv=1
" Open splits at the bottom
let g:netrw_alto=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set hidden at no to disable some error messages
set nohidden
" Always show cursorline "
set cursorline
" Always show current position
set ruler
" List char by default
set list
" Height of the command bar
set cmdheight=1
" Remember more commands and search history
set history=1000
" Use many muchos levels of undo
set undolevels=1000
" Change the terminal's title
set title
" Don't beep
set visualbell
" Don't beep
set noerrorbells
" Desactivate mouse by default
set mouse = ""
" Show cmd
set showcmd
" Visual autocomplete for command menu
set wildmenu
set wildignore=*.swp,*.bak,*.pyc,*.class
" Provides tab-completion for all file-related Tasks
set path+=**
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Theme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set laststatus=2
" Enable syntax highlighting
if &t_Co >= 256 || has("gui_running")
    syntax on
    highlight StatusLineNC ctermbg=None ctermfg=None cterm=None
    highlight StatusLine ctermbg=Black ctermfg=None cterm=None
    highlight VertSplit ctermbg=None ctermfg=None cterm=None
    highlight TabLineSel ctermbg=None ctermfg=White cterm=None
    highlight TabLine ctermbg=Black ctermfg=White cterm=None
    highlight TabLineFill ctermbg=None ctermfg=White cterm=None
    highlight FoldColumn ctermbg=None
    highlight EndOfBuffer ctermfg=Black guifg=None
endif

if &t_Co > 2 || has("gui_running")
    " switch syntax highlighting on, when the terminal has colors
    syntax on
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vsplit on ctags with C-\
map <C-\> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" Toggle list on/off
nnoremap <silent> <C-k><C-l> :set list! \|  ToggleWhitespace<cr>
inoremap <silent> <C-k><C-l> <C-o> :set list! \| ToggleWhitespace<cr>

" Toggle relative number on/off
nnoremap <silent> <c-k><c-n> :set relativenumber!<cr>
inoremap <silent> <c-k><c-n> <c-o> :set relativenumber!<cr>

" Toggle cursorline on/off
map <C-k><C-v> :set cursorline!<cr>

" Spellcheck
map <silent> <F7> "<Esc>:silent setlocal spell! spelllang=en<CR>"
map <silent> <F6> "<Esc>:silent setlocal spell! spelllang=fr<CR>"
map <F8> [s
map <F9> ]s

" Deselect text
nnoremap <silent> <C-k><C-d> :noh<cr>

" Convert tab to spaces
nnoremap <silent> <C-k><C-f> :execute "%s/\t/    /g"<cr>

" Clean all whitespaces
nnoremap <silent> <C-k><C-s> :call StripTrailingWhitespace() <cr>

" Mapping for NERD Commenter
nnoremap <silent> <C-k><C-x> :call NERDComment(0, "toggle")<cr>
inoremap <C-k><C-x> <C-o>:call NERDComment(0, "toggle")<cr>
vnoremap <silent> <C-k><C-x> :call NERDComment(0, "toggle")<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Language specific configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup configgroup
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>100v.\+', -1)
    hi m2 guibg=red ctermbg=red

    "autocmd FileType *.py setlocal formatprg=par\ -w74\ -T4
    autocmd BufNewFile,BufRead *.py setlocal tabstop=4
    autocmd BufNewFile,BufRead *.py setlocal softtabstop=4
    autocmd BufNewFile,BufRead setlocal shiftwidth=4
    autocmd BufNewFile,BufRead setlocal textwidth=79
    autocmd BufNewFile,BufRead setlocal expandtab
    autocmd BufNewFile,BufRead setlocal autoindent
    autocmd BufNewFile,BufRead setlocal fileformat=unix

    autocmd FileType sh     setlocal tabstop=2
    autocmd FileType sh     setlocal shiftwidth=2
    autocmd FileType sh     setlocal softtabstop=2

    autocmd FileType tex     setlocal tabstop=4
    autocmd FileType tex     setlocal shiftwidth=4
    autocmd FileType tex     setlocal expandtab
    autocmd FileType tex     setlocal softtabstop=4

    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufRead,BufNewFile *.bb* set filetype=sh
augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! StripTrailingWhitespace()
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
endfunction
