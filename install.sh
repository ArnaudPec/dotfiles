#!/usr/bin/env sh

DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SOURCES_DIR=$DOTFILES_DIR/src
alias apt="apt --assume-yes"

install_cava()
{
    cd "$SOURCES_DIR" || exit 1
    git clone https://github.com/karlstav/cava
    sudo apt-get install libfftw3-dev libasound2-dev libncursesw5-dev \
      libpulse-dev libtool fftw-dev
    cd cava || exit 1
    ./autogen.sh
    ./configure --disable-legacy_iniparser
    make
    sudo make install
    cd .. || exit 1
    rm -rf cava
    cd "$DOTFILES_DIR" || exit 1
    stow cava
}

install_pamixer()
{
    cd "$SOURCES_DIR" || exit 1
    sudo apt install libpulse-dev libboost-program-options-dev
    git clone https://github.com/cdemoulins/pamixer.git
    cd pamixer || exit 1
    make
    sudo make install
    cd .. || exit 1
    rm -rf pamixer
    cd "$DOTFILES_DIR" || exit 1
}

install_rofi()
{
    cd "$SOURCES_DIR" || exit 1
    git clone https://github.com/DaveDavenport/rofi
    git submodule update --init

    sudo apt install bison libpangocairo-1.0-0 libcairo2-dev libbxkbcommon \
    libxkbcommon-x11-dev libxcb-xkb-dev libxcb-xrm-dev flex \
    libpango1.0-dev libpangocairo-1.0-0 libstartup-notification0-dev \
    librsvg2-dev check

    autorecong -i
    ./configure --disable-check
    make
    sudo make install
    cd .. || exit 1
    rm -rf rofi
    cd "$DOTFILES_DIR" || exit 1
}

install_minimal()
{
    sudo apt install man git fish zsh sudo stow highlight make \
    gcc automake libncurses-dev pkg-config tree python curl
}

install_extra()
{
    sudo apt install moc i3lock zathura mpv feh firefox lm-sensors acpi
}

install_comptom()
{
    sudo apt install compton
    cd "$DOTFILES_DIR" || exit 1
    stown compton
}

install_bspwm()
{
    cd "$SOURCES_DIR" || exit 1
    sudo apt-get install libxcb-util0-dev libxcb-ewmh-dev libxcb-randr0-dev \
    libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-xinerama0-dev libasound2-dev \
    libxcb-xtest0-dev xcb

    git clone https://github.com/baskerville/bspwm.git
    git clone https://github.com/baskerville/sxhkd.git

    cd bspwm || exit 1
    make
    sudo make install
    sudo cp contrib/freedesktop/bspwm.desktop /usr/share/xsessions/
    cd .. || exit 1

    cd sxhkd || exit 1
    make
    sudo make install
    cd .. || exit 1
    rm -rf sxhkd bspwm
    cd "$DOTFILES_DIR" || exit 1

    stow bspwm
    stow sxhdk

    mkdir -p ~/Pictures/Wallpapers
    cd ~/Pictures/Wallpapers || exit 1
    wget https://cdn.dribbble.com/users/108482/screenshots/1355879/attachments/193117/Space-Desktop.jpg -O space.jpg
    cd DOTFILES_DIR || exit 1
}

install_urxvt()
{
    sudo apt install wmctrl rxvt-unicode
    stow colorschemes
    stow X
    xrdb -merge ~/.Xresources
    wget https://raw.githubusercontent.com/effigies/urxvt-perl/master/fullscreen
    sudo cp fullscreen /usr/lib/urxvt/perl/
    rm -rf fullscreen
}

install_tmux()
{
    cd "$SOURCES_DIR" || exit 1
    sudo apt install libevent-dev
    git clone https://github.com/tmux/tmux.git
    cd tmux || exit 1
    sh autogen.sh
    ./configure && make
    sudo make install
    cd .. || exit 1
    rm -rf tmux
    cd "$DOTFILES_DIR" || exit 1
    stow tmux
}

install_neovim()
{
    sudo apt install neovim ctags
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    sudo apt install python-pip
    sudo pip install neovim
    stow nvim
}

install_fish_plugin()
{
    cd "$SOURCES_DIR" || exit 1
    git clone https://github.com/jbonjean/re-search
    cd re-search || exit 1
    make
    sudo cp re-search /usr/bin
    cp re_search.fish ~/.config/fish/functions/
    echo "bind \cr re_search" > ~/.config/fish/functions/fish_user_key_bindings.fish
    cd .. || exit 1
    rm -rf re-search
    cd "$DOTFILES_DIR" || exit 1
}

install_minimal

if [[ $(basename $SHELL) != "zsh" ]]; then

    echo "
    sudo apt install zsh git

    zsh
    git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
    setopt EXTENDED_GLOB
    for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
    ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
    done

    chsh -s /bin/zsh
    "
else
    cd "$DOTFILES_DIR" || exit 1
    mkdir -p "$SOURCES_DIR"
    echo " now available:

    install_minimal
    install_extra
    install_cava
    install_pamixer
    install_rofi
    install_compton
    install_urxvt
    install_bspwm
    install_neovim
    install_tmux
"
fi
